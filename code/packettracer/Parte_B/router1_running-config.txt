!
version 15.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname R1
!
!
!
!
!
!
!
!
no ip cef
ipv6 unicast-routing
!
no ipv6 cef
!
!
!
!
license udi pid CISCO2901/K9 sn FTX1524WKF0-
!
!
!
!
!
!
!
!
!
!
!
spanning-tree mode pvst
!
!
!
!
!
!
interface Loopback0
 no ip address
 ipv6 address 2001:0:0:10::1/64
 ipv6 enable
 ipv6 ospf cost 1
 ipv6 ospf hello-interval 5
 ipv6 ospf dead-interval 20
 ipv6 ospf priority 0
 ipv6 ospf 10 area 0
!
interface GigabitEthernet0/0
 no ip address
 duplex auto
 speed auto
 ipv6 address 2001:0:0:3::1/64
 ipv6 enable
 ipv6 ospf cost 1
 ipv6 ospf hello-interval 5
 ipv6 ospf dead-interval 20
 ipv6 ospf priority 1
 ipv6 ospf 10 area 0
!
interface GigabitEthernet0/1
 no ip address
 duplex auto
 speed auto
 ipv6 address 2001:0:0:4::1/64
 ipv6 enable
 ipv6 ospf cost 1
 ipv6 ospf hello-interval 5
 ipv6 ospf dead-interval 20
 ipv6 ospf priority 1
 ipv6 ospf 10 area 1
!
interface Vlan1
 no ip address
 shutdown
!
ipv6 router ospf 10
 router-id 0.0.0.10
 log-adjacency-changes
!
ip classless
!
ip flow-export version 9
!
!
!
no cdp run
!
!
!
!
!
!
line con 0
!
line aux 0
!
line vty 0 4
 login
!
!
!
end

